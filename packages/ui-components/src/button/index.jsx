// @flow
import * as React from 'react'
import classnames from 'classnames'
import styles from './styles.scss'

import { Icon } from '../../'

type TProps = {
  type: 'button' | 'submit' | 'reset',
  theme?: 'primary' | 'default' | 'success' | 'danger' | 'link',
  block?: boolean,
  loading?: boolean,
  active?: boolean,
  disabled?: boolean,
  icon?: string,
  actAsAnchor?: boolean,
  onClick?: Function,
  children?: any,
}

type TState = {
  minWidth: ?number
}

class Button extends React.PureComponent<TProps, TState> {
  ref: ?any

  static defaultProps = {
    type: 'button',
    theme: 'default',
    block: false,
    loading: false,
    active: false,
    disabled: false,
    icon: null,
    actAsAnchor: false,
    onClick: false
  }

  state = {
    minWidth: null
  }

  get styleProps (): Object {
    const { minWidth } = this.state
    const styleProps = {}
    if (minWidth) styleProps.minWidth = minWidth

    return styleProps
  }

  /**
   * @description Return the element type based on the actAsAnchor prop
   * @returns { Void }
   */
  get elementTag (): string {
    const { actAsAnchor } = this.props
    return actAsAnchor ? 'a' : 'button'
  }

  /**
   * @description If the element is a button, set the type attr
   * @returns { Void }
   */
  get type (): string {
    const tag = this.elementTag
    return tag === 'button' ? this.props.type : undefined
  }

  /**
   * @description Initialize ref for the element
   * @param { Object } ref - HTMLTagElement
   * @returns { Void }
   */
  handleButtonRef = (ref): void => {
    this.ref = ref

    /* istanbul ignore if  */
    if (ref) {
      const minWidth = ref.offsetWidth
      this.setState({ minWidth })
    }
  }

  /**
   * @description Callback after click. Will not fire if disabled
   * @returns { Void }
   */
  handleClick = (): void => {
    const { onClick, disabled } = this.props
    !disabled && onClick && onClick()
  }

  render (): React.Node {
    const {
      actAsAnchor,
      type,
      theme,
      block,
      loading,
      active,
      disabled,
      icon,
      onClick,
      children,
      className,
      ...remainingProps
    } = this.props

    const containerClass = classnames(
      styles.button,
      [styles[theme]],
      { [styles.block]: block },
      { [styles.active]: active || loading },
      { [styles.disabled]: disabled },
      className
    )

    const ButtonTag = this.elementTag

    return (
      <ButtonTag
        type={this.type}
        disabled={disabled}
        className={containerClass}
        onClick={this.handleClick}
        ref={this.handleButtonRef}
        {...remainingProps}
        style={this.styleProps}
      >
        {loading ? (
          <Icon name="spinner-third" size={15} spin />
        ) : (
          <div className={styles.buttonInner}>
            {icon && (
              <Icon name={icon} size={14} />
            )}
            {children && (
              <div className={styles.buttonContent}>
                {children}
              </div>
            )}
          </div>
        )}
      </ButtonTag>
    )
  }
}

export { styles }
export default Button
