import * as React from 'react'
import Button, { styles } from './'

let props, wrapper
describe('<Button theme="success" />', () => {
  beforeEach(() => {
    props = {
      theme: 'success',
      children: 'Lorem ipsum',
      onClick: jest.fn()
    }

    wrapper = shallow(<Button {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper).toHaveLength(1)
    expect(wrapper).toMatchSnapshot()
  })

  it('should have the correctly className', () => {
    expect(wrapper.props().className).toEqual([styles.button, styles.success].join(' '))
  })

  it('should render the right content', () => {
    expect(wrapper.text()).toEqual(props.children)
  })

  it('should call the click handler', () => {
    expect(props.onClick).toHaveBeenCalledTimes(0)
    wrapper.simulate('click')
    expect(props.onClick).toHaveBeenCalledTimes(1)
  })

  it('should return the correct style props', () => {
    expect(wrapper.instance().styleProps).toEqual({})
    wrapper.setState({ minWidth: 150 })
    expect(wrapper.instance().styleProps).toEqual({ minWidth: 150 })
  })
})

describe('<Button disabled', () => {
  beforeEach(() => {
    props = {
      children: 'Lorem ipsum',
      onClick: jest.fn(),
      disabled: true
    }

    wrapper = shallow(<Button {...props} />)
  })

  it('should have the disabled prop', () => {
    expect(wrapper.props().disabled).toBe(true)
  })

  it('should have the correctly className', () => {
    expect(wrapper.props().className).toEqual([styles.button, styles.default, styles.disabled].join(' '))
  })

  it('should call the click handler', () => {
    expect(props.onClick).toHaveBeenCalledTimes(0)
    wrapper.simulate('click')
    expect(props.onClick).toHaveBeenCalledTimes(0)
  })
})

describe('<Button active', () => {
  beforeEach(() => {
    props = {
      children: 'Lorem ipsum',
      onClick: jest.fn(),
      active: true
    }

    wrapper = shallow(<Button {...props} />)
  })

  it('should have the correctly className', () => {
    expect(wrapper.props().className).toEqual([styles.button, styles.default, styles.active].join(' '))
  })
})

describe('<Button block', () => {
  beforeEach(() => {
    props = {
      children: 'Lorem ipsum',
      onClick: jest.fn(),
      block: true,
      theme: 'danger'
    }

    wrapper = shallow(<Button {...props} />)
  })

  it('should have the correctly className', () => {
    expect(wrapper.props().className).toEqual([
      styles.button,
      styles.danger,
      styles.block
    ].join(' '))
  })
})

describe('<Button loading', () => {
  beforeEach(() => {
    props = {
      children: 'Lorem ipsum'
    }

    wrapper = mount(<Button {...props} />)
  })

  it('should not show the spinner when its not loading', () => {
    const hasSpinner = wrapper.find('Icon').length
    expect(hasSpinner).toEqual(0)
  })

  it('should show the spinner when its loading', () => {
    wrapper.setProps({ loading: true })
    const hasSpinner = wrapper.find('i').length
    expect(hasSpinner).toEqual(1)
  })
})

describe('<Button actAsAnchor', () => {
  beforeEach(() => {
    props = {
      actAsAnchor: true
    }

    wrapper = shallow(<Button {...props} />)
  })

  it('should be an anchor', () => {
    const elementTag = wrapper.instance().elementTag
    expect(elementTag).toEqual('a')
  })
})

describe('<Button with custom class', () => {
  beforeEach(() => {
    props = {
      className: 'lorem',
      children: 'foo dolor'
    }

    wrapper = shallow(<Button {...props} />)
  })

  it('should set the correct class', () => {
    expect(wrapper.props().className).toEqual([
      styles.button,
      styles.default,
      'lorem'
    ].join(' '))
  })
})

describe('<Button with icon', () => {
  beforeEach(() => {
    props = {
      icon: 'envelope'
    }

    wrapper = mount(<Button {...props} />)
  })

  it('should render an icon and not render any content', () => {
    expect(wrapper.find('i').length).toBe(1)
    expect(wrapper.find(styles.buttonContent).length).toBe(0)
  })
})
