/* @flow */
import * as React from 'react'
import classnames from 'classnames'
import { ICON_BASE_PREFIX, FAMILY_PREFIX_MAP } from './constants'
import styles from './styles.scss'

type TProps = {
  name: string,
  size: ?number,
  color: ?string,
  spin?: boolean,
  className: ?any,
  type: 'regular' | 'light' | 'solid'
}

class Icon extends React.Component<TProps> {
  static defaultProps = {
    type: 'regular',
    spin: false,
    size: 14
  }

  /**
   * @description return the font awesome prefix according with the family type
   * @returns { String }
   */
  get familyPrefix (): string {
    return FAMILY_PREFIX_MAP[this.props.type]
  }

  /**
   * @description return the spin class
   * @returns { String }
   */
  get spinClass (): ?string {
    const { spin } = this.props
    if (!spin) return undefined
    return styles.spin
  }

  /**
   * @description shortcuts to set a css style prop
   * @returns { Object }
   */
  get styleProp (): Object {
    const { size, color } = this.props
    return { fontSize: size, color }
  }

  render (): React.Node {
    const { name, className } = this.props

    const iconClassName = classnames(
      `${this.familyPrefix} ${ICON_BASE_PREFIX}-${name}`,
      this.spinClass,
      className
    )

    return <i className={iconClassName} style={this.styleProp} />
  }
}

export default Icon
