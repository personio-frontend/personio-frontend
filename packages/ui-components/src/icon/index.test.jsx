import React from 'react'
import { shallow } from 'enzyme'
import Icon from './'
import { ICON_BASE_PREFIX } from './constants'

let wrapper
let props

describe('<Icon />', () => {
  beforeEach(() => {
    props = { name: 'pencil' }
    wrapper = shallow(<Icon {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper).toMatchSnapshot()
    expect(wrapper).toHaveLength(1)
  })

  it('should have the right classname', () => {
    expect(wrapper.props().className).toEqual(`far ${ICON_BASE_PREFIX}-${props.name}`)
  })
})

describe('<Icon type="light" />', () => {
  beforeEach(() => {
    props = { name: 'pencil', type: 'light' }
    wrapper = shallow(<Icon {...props} />)
  })

  it('should have the right classname', () => {
    expect(wrapper.props().className).toEqual(`fal ${ICON_BASE_PREFIX}-${props.name}`)
  })
})

describe('<Icon type="solid" />', () => {
  beforeEach(() => {
    props = { name: 'pencil', type: 'solid' }
    wrapper = shallow(<Icon {...props} />)
  })

  it('should have the right classname', () => {
    expect(wrapper.props().className).toEqual(`fas ${ICON_BASE_PREFIX}-${props.name}`)
  })
})

describe('<Icon name="spinner" spin />', () => {
  beforeEach(() => {
    props = { name: 'spinner', spin: true }
    wrapper = shallow(<Icon {...props} />)
  })

  it('should have the right classname', () => {
    expect(wrapper.props().className).toEqual(`far ${ICON_BASE_PREFIX}-${props.name} spin`)
  })
})
