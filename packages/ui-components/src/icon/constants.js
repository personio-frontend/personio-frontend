export const ICON_BASE_PREFIX = 'fa'

export const FAMILY_PREFIX_MAP = {
  regular: 'far',
  light: 'fal',
  solid: 'fas'
}
