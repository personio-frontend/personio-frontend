// @flow
import * as React from 'react'
import classnames from 'classnames'
import times from 'lodash/times'
import flatten from 'lodash/flatten'
import styles from './styles.scss'

const TABLES_COUNT = 2

type TProps = {
  columnsFixed?: number,
  forceHorizontalScroll?: boolean,
  size: 'large' | 'medium' | 'small',
  containerClassName?: any,

  children?: React.Node
};

type TState = {
  hoveredRowId: ?(string | number)
}

class Table extends React.Component<TProps, TState> {
  static defaultProps = {
    columnsFixed: 0,
    forceHorizontalScroll: false,
    size: 'medium',
    containerClassName: null,
    children: null
  }

  state = {
    hoveredRowId: null
  };

  handleFixedContainerRef = (ref: HTMLDivElement): void => {
    /* istanbul ignore if  */
    if (ref) ref.nextSibling.style.marginLeft = `${ref.offsetWidth}px`
  }

  handleMouseEnter = (rowId: string | number): void => {
    this.setState({ hoveredRowId: rowId })
  }

  handleMouseLeave = (): void => {
    this.setState({ hoveredRowId: null })
  }

  generateFixedAndNotFixedTables = (): Array<React.Node> => {
    const { fixed, size, forceHorizontalScroll, children } = this.props
    const tableHeadOrBodyExtraProps = { fixed, size, forceHorizontalScroll }
    const tableRowBodyExtraProps = {
      onHover: this.handleMouseEnter,
      onHoverLeave: this.handleMouseLeave,
      hoveredRowId: this.state.hoveredRowId
    }

    return times(TABLES_COUNT).map((index) => {
      return React.Children.map(children, (tableHeadOrBody) => {
        const { displayName } = tableHeadOrBody.type

        return React.cloneElement(tableHeadOrBody, {
          ...tableHeadOrBodyExtraProps,
          children: React.Children.map(tableHeadOrBody.props.children, (tableRow) => {
            const extraProps = displayName === 'TableBody' ? tableRowBodyExtraProps : {}

            if (React.isValidElement(tableRow)) {
              const tableRowChildren = flatten(tableRow.props.children)

              return React.cloneElement(tableRow, {
                ...extraProps,
                children: this.spliceCells(tableRowChildren)[index]
              })
            }
          })
        })
      })
    })
  }

  spliceCells = (cells: Array<React.Node>): Array<React.Node> => {
    const { columnsFixed } = this.props

    const fixedColumns = cells.slice(0, columnsFixed)
    const notFixedColumns = cells.slice(columnsFixed, cells.length)

    return [fixedColumns, notFixedColumns]
  }

  render (): React.Node {
    const {
      columnsFixed,
      forceHorizontalScroll,
      size,
      children
    } = this.props

    const containerClassName = classnames(
      styles.container,
      this.props.containerClassName
    )

    if (!columnsFixed) {
      return (
        <div className={containerClassName}>
          <table className={styles.tableContainer}>
            {React.Children.map(children, child =>
              React.cloneElement(child, { forceHorizontalScroll, size })
            )}
          </table>
        </div>
      )
    }

    const [
      TableFixed,
      TableNotFixed
    ] = this.generateFixedAndNotFixedTables()

    return (
      <div className={containerClassName}>
        <div className={styles.tableFixedContainer} ref={this.handleFixedContainerRef}>
          <table className={styles.tableContainer}>
            {TableFixed}
          </table>
        </div>
        <div className={styles.tableNotFixedContainer}>
          <table className={styles.tableContainer}>
            {TableNotFixed}
          </table>
        </div>
      </div>
    )
  }
}

export { styles }
export default Table
