import React from 'react'
import times from 'lodash/times'
import Table from '../'
import { styles } from './'

let wrapper
let props

const children = properties => (
  <Table {...properties}>
    <Table.Head>
      <Table.Row>
        <Table.Cell>Attributes</Table.Cell>
        {times(5).map(i => (
          <Table.Cell key={i}>Header {i}</Table.Cell>
        ))}
      </Table.Row>
    </Table.Head>
    <Table.Body>u
      {times(3).map(i => (
        <Table.Row id={i} key={i}>
          <Table.Cell>IBAN</Table.Cell>
          {times(5).map(i => (
            <Table.Cell key={i}>Cell {i}</Table.Cell>
          ))}
        </Table.Row>
      ))}
    </Table.Body>
  </Table>
)

describe('<Table />', () => {
  beforeEach(() => {
    props = {}
    wrapper = shallow(children(props))
  })

  it('should render correctly', () => {
    expect(wrapper).toMatchSnapshot()
    expect(wrapper).toHaveLength(1)
    expect(wrapper.find('table')).toHaveLength(1)
  })
})

describe('<Table columnsFixed={3} />', () => {
  beforeEach(() => {
    props = {
      columnsFixed: 3
    }

    wrapper = mount(children(props))
  })

  it('should have 2 tables', () => {
    expect(wrapper.find('table')).toHaveLength(2)
  })

  it('should splice the cells correctly', () => {
    const fakeRows = [1, 2, 3, 4, 5, 6]
    const result = wrapper.instance().spliceCells(fakeRows)

    expect(result).toBeInstanceOf(Array)
    expect(result).toHaveLength(2)
    expect(result[0]).toEqual([1, 2, 3])
    expect(result[1]).toEqual([4, 5, 6])

    const fixedTableFirstRow = wrapper.find('table').first().find('thead tr').first()
    expect(fixedTableFirstRow.find('th').length).toEqual(3)

    const notFixedTableFirstRow = wrapper.find('table').last().find('thead tr').first()
    expect(notFixedTableFirstRow.find('th').length).toEqual(3)
  })

  it('should generate 2 tables', () => {
    const result = wrapper.instance().generateFixedAndNotFixedTables()

    expect(result).toBeInstanceOf(Array)
    expect(result).toHaveLength(2)
  })

  it('should change the state after mouse enter', () => {
    wrapper.instance().handleMouseEnter(1)
    expect(wrapper.state().hoveredRowId).toEqual(1)
  })

  it('should change the state after mouse out', () => {
    wrapper.instance().handleMouseLeave()
    expect(wrapper.state().hoveredRowId).toBeNull()
  })
})

describe('<Table with a custom container classname />', () => {
  beforeEach(() => {
    props = {
      containerClassName: 'lorem'
    }

    wrapper = shallow(children(props))
  })

  it('should render with the correct class', () => {
    expect(wrapper.props().className).toEqual([
      styles.container,
      'lorem'
    ].join(' '))
  })
})
