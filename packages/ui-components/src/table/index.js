import Table from './table'
import TableHead from './table-head'
import TableBody from './table-body'
import TableRow from './table-row'
import TableCell from './table-cell'

// The lines bellow are to make not necessary import all the
// 5 components. You can just import table and use this syntax: <Table.Head />
Table.Head = TableHead
Table.Body = TableBody
Table.Row = TableRow
Table.Cell = TableCell

export { TableHead, TableBody, TableRow, TableCell }
export default Table
