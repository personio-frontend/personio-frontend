import React from 'react'
import TableBody, { styles } from './'

let wrapper
let props

describe('<TableBody />', () => {
  beforeEach(() => {
    props = {}
    wrapper = shallow(
      <TableBody {...props} />
    )
  })

  it('should render correctly', () => {
    expect(wrapper).toMatchSnapshot()
    expect(wrapper).toHaveLength(1)
  })
})

describe('<TableBody with a custom class />', () => {
  beforeEach(() => {
    props = {
      className: 'lorem'
    }

    wrapper = shallow(
      <TableBody {...props} />
    )
  })

  it('should render correctly', () => {
    expect(wrapper.props().className).toEqual([
      styles.tableBodyContainer,
      'lorem'
    ].join(' '))
  })
})
