// @flow
import * as React from 'react'
import classnames from 'classnames'
import styles from './styles.scss'

type TProps = {
  loading?: boolean,
  noResults?: boolean,
  noResultsMessage?: string,
  noResultsIcon?: string,
  renderNoResults?: Function,
  forceHorizontalScroll?: boolean,
  className?: any,
  children?: React.Node,
}

class TableBody extends React.PureComponent<TProps> {
  static defaultProps = {
    loading: false,
    noResults: false,
    className: null,
    forceHorizontalScroll: false
  }

  render (): React.Node {
    const { className, children } = this.props

    const containerClassName = classnames(
      styles.tableBodyContainer,
      className
    )

    return (
      <tbody className={containerClassName}>
        {children}
      </tbody>
    )
  }
}

export { styles }
export default TableBody
