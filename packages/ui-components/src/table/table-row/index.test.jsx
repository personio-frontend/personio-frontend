import React from 'react'
import TableRow, { styles } from './'
import TableCell from '../table-cell'

let wrapper
let props

describe('<TableRow />', () => {
  beforeEach(() => {
    props = {
      id: 1,
      onHover: jest.fn(),
      onHoverLeave: jest.fn(),
      onClick: jest.fn().mockReturnValue({ })
    }

    wrapper = shallow(
      <TableRow {...props}>
        <TableCell>Lorem</TableCell>
      </TableRow>
    )
  })

  it('should render correctly', () => {
    expect(wrapper).toMatchSnapshot()
    expect(wrapper).toHaveLength(1)
  })

  it('should not append a custom insideHeader prop on their children', () => {
    expect(wrapper.children().first().props().insideHeader).toBe(false)
    expect(wrapper.children().last().props().insideHeader).toBe(false)
  })

  it('should indicate the row is not active', () => {
    expect(wrapper.instance().isHovered).toBe(false)
  })

  it('should call the handlers', () => {
    wrapper.simulate('mouseEnter')
    wrapper.simulate('mouseLeave')
    wrapper.simulate('click')

    expect(props.onHover).toHaveBeenCalledTimes(1)
    expect(props.onHover).toHaveBeenCalledWith(props.id)
    expect(props.onHoverLeave).toHaveBeenCalledTimes(1)
    expect(props.onHoverLeave).toHaveBeenCalledWith(props.id)
    expect(props.onClick).toHaveBeenCalledTimes(1)
  })
})

describe('<TableRow insideHeader />', () => {
  beforeEach(() => {
    props = {
      insideHeader: true
    }

    wrapper = shallow(
      <TableRow {...props}>
        <TableCell>Lorem</TableCell>
        <TableCell>Lorem</TableCell>
      </TableRow>
    )
  })

  it('should append a custom insideHeader prop on their children', () => {
    expect(wrapper.children().first().props().insideHeader).toBe(true)
    expect(wrapper.children().last().props().insideHeader).toBe(true)
  })

  it('should have the correctly className', () => {
    expect(wrapper.props().className).toEqual([
      styles.tableRowContainer,
      styles.light,
      styles.medium,
      styles.insideHeader
    ].join(' '))
  })
})

describe('<TableRow with custom class />', () => {
  beforeEach(() => {
    props = {
      className: 'lorem'
    }

    wrapper = shallow(
      <TableRow {...props}>
        <TableCell>Lorem</TableCell>
        <TableCell>Lorem</TableCell>
      </TableRow>
    )
  })

  it('should have the correctly className', () => {
    expect(wrapper.props().className).toEqual([
      styles.tableRowContainer,
      styles.light,
      styles.medium,
      'lorem'
    ].join(' '))
  })
})

describe('<TableRow disabled />', () => {
  beforeEach(() => {
    props = {
      disabled: true
    }

    wrapper = shallow(
      <TableRow {...props}>
        <TableCell>Lorem</TableCell>
        <TableCell>Lorem</TableCell>
      </TableRow>
    )
  })

  it('should have the correctly className', () => {
    expect(wrapper.props().className).toEqual([
      styles.tableRowContainer,
      styles.light,
      styles.medium,
      'disabled'
    ].join(' '))
  })
})

describe('<TableRow with a custom theme />', () => {
  beforeEach(() => {
    props = {
      theme: 'danger'
    }

    wrapper = shallow(
      <TableRow {...props}>
        <TableCell>Lorem</TableCell>
        <TableCell>Lorem</TableCell>
      </TableRow>
    )
  })

  it('should have the correctly className', () => {
    expect(wrapper.props().className).toEqual([
      styles.tableRowContainer,
      styles.danger,
      styles.medium
    ].join(' '))
  })
})

describe('<TableRow with a custom size />', () => {
  beforeEach(() => {
    props = {
      size: 'large'
    }

    wrapper = shallow(
      <TableRow {...props}>
        <TableCell>Lorem</TableCell>
        <TableCell>Lorem</TableCell>
      </TableRow>
    )
  })

  it('should have the correctly className', () => {
    expect(wrapper.props().className).toEqual([
      styles.tableRowContainer,
      styles.light,
      styles.large
    ].join(' '))
  })
})

describe('<TableRow noBorder />', () => {
  beforeEach(() => {
    props = {
      noBorder: true
    }

    wrapper = shallow(
      <TableRow {...props}>
        <TableCell>Lorem</TableCell>
        <TableCell>Lorem</TableCell>
      </TableRow>
    )
  })

  it('should have the correctly className', () => {
    expect(wrapper.props().className).toEqual([
      styles.tableRowContainer,
      styles.light,
      styles.medium,
      styles.noBorder
    ].join(' '))
  })
})

describe('<TableRow fixed />', () => {
  beforeEach(() => {
    props = {
      fixed: true
    }

    wrapper = shallow(
      <TableRow {...props}>
        <TableCell>Lorem</TableCell>
        <TableCell>Lorem</TableCell>
      </TableRow>
    )
  })

  it('should have the correctly className', () => {
    expect(wrapper.props().className).toEqual([
      styles.tableRowContainer,
      styles.light,
      styles.medium,
      styles.fixed
    ].join(' '))
  })
})

describe('<TableRow activated />', () => {
  beforeEach(() => {
    props = {
      id: 1,
      hoveredRowId: 1
    }

    wrapper = shallow(
      <TableRow {...props}>
        <TableCell>Lorem</TableCell>
        <TableCell>Lorem</TableCell>
      </TableRow>
    )
  })

  it('should indicate the row is active', () => {
    expect(wrapper.instance().isHovered).toBe(true)
  })
})

describe('<TableRow backgroundOnHover="red" />', () => {
  beforeEach(() => {
    props = {
      id: 1,
      hoveredRowId: 1,
      backgroundOnHover: 'red'
    }

    wrapper = shallow(
      <TableRow {...props}>
        <TableCell>Lorem</TableCell>
        <TableCell>Lorem</TableCell>
      </TableRow>
    )
  })

  it('should have a style prop with the red bg when is active', () => {
    expect(wrapper.instance().styleProps).toEqual({ background: props.backgroundOnHover })
    expect(wrapper.props().style.background).toEqual(props.backgroundOnHover)
  })

  it('should not have a style prop with red when is not active', () => {
    wrapper.setProps({ hoveredRowId: 2 })
    expect(wrapper.instance().styleProps).toEqual({})
    expect(wrapper.props().style.background).toBeUndefined()
  })

  it('should not have a style prop when is active but does not have a backgroundOnHover prop', () => {
    wrapper.setProps({ backgroundOnHover: undefined })
    expect(wrapper.instance().styleProps).toEqual({})
    expect(wrapper.props().style.background).toBeUndefined()
  })
})
