// @flow
import * as React from 'react'
import classnames from 'classnames'
import styles from './styles.scss'

type TProps = {
  id: string | number,
  hoveredRowId?: string | number,
  theme?: 'default' | 'primary' | 'success' | 'info' | 'warning' | 'danger' | 'light',
  size: 'small' | 'medium' | 'large',
  disabled?: boolean,
  noBorder?: boolean,
  fixed?: boolean,
  backgroundOnHover: ?string,
  insideHeader?: boolean,
  className?: any,
  children?: React.Node,

  // Events
  onClick: ?Function,
  onHover: ?Function,
  onHoverLeave: ?Function
}

class TableRow extends React.PureComponent<TProps> {
  static defaultProps = {
    hoveredRowId: null,
    theme: 'light',
    size: 'medium',
    disabled: false,
    noBorder: false,
    fixed: false,
    backgroundOnHover: null,
    insideHeader: false,
    className: null,

    onClick: undefined,
    onHover: undefined,
    onHoverLeave: undefined
  }

  get isHovered (): boolean {
    const { id, hoveredRowId } = this.props
    return String(hoveredRowId) === String(id)
  }

  get styleProps (): Object {
    const { backgroundOnHover } = this.props
    let props = {}

    if (this.isHovered && backgroundOnHover) {
      props.background = backgroundOnHover
    }

    return props
  }

  handleMouseEnter = (): void => {
    const { onHover, id } = this.props
    onHover && onHover(id)
  }

  handleMouseLeave = (): void => {
    const { onHoverLeave, id } = this.props
    onHoverLeave && onHoverLeave(id)
  }

  render (): React.Node {
    const {
      id,
      hoveredRowId,
      theme,
      size,
      disabled,
      noBorder,
      fixed,
      backgroundOnHover,
      insideHeader,
      className,
      onClick,
      onHover,
      onHoverLeave,

      children,
      ...rest
    } = this.props

    const containerClassName = classnames(
      styles.tableRowContainer,
      [styles[theme]],
      [styles[size]],
      { [styles.disabled]: disabled },
      { [styles.noBorder]: noBorder },
      { [styles.fixed]: fixed },
      { [styles.insideHeader]: insideHeader },
      className
    )

    return (
      <tr
        {...rest}
        onMouseEnter={this.handleMouseEnter}
        onFocus={this.handleMouseEnter}
        onMouseLeave={this.handleMouseLeave}
        onClick={disabled ? undefined : onClick}
        className={containerClassName}
        style={this.styleProps}
      >
        {React.Children.map(children, (child) =>
          React.cloneElement(child, { insideHeader })
        )}
      </tr>
    )
  }
}

export { styles }
export default TableRow
