// @flow
import * as React from 'react'
import classnames from 'classnames'
import styles from './styles.scss'

type TProps = {
  className?: any,
  children?: React.Node
}

class TableHead extends React.Component<TProps> {
  static defaultProps = {
    className: null
  }

  render (): React.Node {
    const { fixed, className, children } = this.props

    const containerClassNames = classnames(
      styles.tableHeadContainer,
      className
    )

    return (
      <thead className={containerClassNames}>
        {React.Children.map(children, child =>
          React.cloneElement(child, { insideHeader: true })
        )}
      </thead>
    )
  }
}

export { styles }
export default TableHead
