import React from 'react'
import TableHead, { styles } from './'

let wrapper
let props

describe('<TableHead />', () => {
  beforeEach(() => {
    props = {}
    wrapper = shallow(
      <TableHead {...props} />
    )
  })

  it('should render correctly', () => {
    expect(wrapper).toMatchSnapshot()
    expect(wrapper).toHaveLength(1)
  })
})

describe('<TableHead with a custom class />', () => {
  beforeEach(() => {
    props = {
      className: 'lorem'
    }

    wrapper = shallow(
      <TableHead {...props} />
    )
  })

  it('should render correctly', () => {
    expect(wrapper.props().className).toEqual([
      styles.tableHeadContainer,
      'lorem'
    ].join(' '))
  })
})
