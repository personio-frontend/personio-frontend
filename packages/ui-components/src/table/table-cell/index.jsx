// @flow
import * as React from 'react'
import classnames from 'classnames'
import { Icon } from '../../'
import styles from './styles.scss'

type TProps = {
  theme?: 'default' | 'primary' | 'success' | 'info' | 'warning' | 'danger' | 'light',
  disabled?: boolean,
  bold?: boolean,
  empty?: boolean,
  width?: number | string,
  minWidth?: number,
  maxWidth?: number,
  insideHeader: boolean,

  onClick?: Function,

  className?: any,
  children?: React.Node | string,
}

class TableCell extends React.PureComponent<TProps> {
  static defaultProps = {
    disabled: false,
    bold: false,
    empty: false,
    insideHeader: false,
    className: null,
    children: '&nbsp;'
  }

  get tagName (): string {
    const { insideHeader } = this.props
    return insideHeader ? 'th' : 'td'
  }

  get styles (): Object {
    let styles = {}
    const { props } = this

    const properties = ['width', 'minWidth', 'maxWidth']
    properties.map(p => {
      if (props[p]) styles[p] = props[p]
    })

    return styles
  }

  handleClick = (event: SyntheticEvent<HTMLElement>): void => {
    const { disabled, onClick } = this.props
    !disabled && onClick && onClick(event)
  }

  render (): React.Node {
    const {
      theme,
      disabled,
      bold,
      empty,
      width,
      minWidth,
      maxWidth,
      insideHeader,
      className,
      children,
      ...rest
    } = this.props

    const TagName = this.tagName

    const containerClassName = classnames(
      { [styles.tdContainer]: TagName === 'td' },
      { [styles.thContainer]: TagName === 'th' },
      { [styles[theme]]: theme },
      { [styles.disabled]: disabled },
      { [styles.bold]: bold },
      className
    )

    return (
      <TagName
        style={this.styles}
        {...rest}
        onClick={this.handleClick}
        className={containerClassName}
      >
        {empty ? (
          <div className={styles.emptyContainer}>
            <Icon name="minus" />
            <Icon name="minus" />
          </div>
        ) : (
          children
        )}
      </TagName>
    )
  }
}

export { styles }
export default TableCell
