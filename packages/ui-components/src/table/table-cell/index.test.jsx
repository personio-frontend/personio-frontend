import React from 'react'
import TableCell, { styles } from './'

let wrapper
let props

describe('<TableCell />', () => {
  beforeEach(() => {
    props = {}
    wrapper = shallow(
      <TableCell {...props}>
        lorem
      </TableCell>
    )
  })

  it('should render correctly', () => {
    expect(wrapper).toMatchSnapshot()
    expect(wrapper).toHaveLength(1)
  })

  it('should be a <td />', () => {
    expect(wrapper.instance().tagName).toEqual('td')
    expect(wrapper.name()).toBe('td')
  })

  it('should render the correct class', () => {
    expect(wrapper.props().className).toEqual([
      styles.tdContainer
    ].join(' '))
  })
})

describe('<TableCell with a custom theme />', () => {
  beforeEach(() => {
    props = {
      theme: 'primary'
    }

    wrapper = shallow(
      <TableCell {...props}>
        lorem
      </TableCell>
    )
  })

  it('should render the correct class', () => {
    expect(wrapper.props().className).toEqual([
      styles.tdContainer,
      styles.primary
    ].join(' '))
  })
})

describe('<TableCell disabled />', () => {
  beforeEach(() => {
    props = {
      disabled: true
    }

    wrapper = shallow(
      <TableCell {...props}>
        lorem
      </TableCell>
    )
  })

  it('should render the correct class', () => {
    expect(wrapper.props().className).toEqual([
      styles.tdContainer,
      styles.disabled
    ].join(' '))
  })
})

describe('<TableCell bold />', () => {
  beforeEach(() => {
    props = {
      bold: true
    }

    wrapper = shallow(
      <TableCell {...props}>
        lorem
      </TableCell>
    )
  })

  it('should render the correct class', () => {
    expect(wrapper.props().className).toEqual([
      styles.tdContainer,
      styles.bold
    ].join(' '))
  })
})

describe('<TableCell with fix width />', () => {
  beforeEach(() => {
    props = {
      width: 150
    }

    wrapper = shallow(
      <TableCell {...props}>
        lorem
      </TableCell>
    )
  })

  it('should set the correct width', () => {
    expect(wrapper.instance().styles).toEqual({ width: props.width })
    expect(wrapper.props().style).toEqual({ width: props.width })
  })
})

describe('<TableCell with a minWidth and maxWidth />', () => {
  beforeEach(() => {
    props = {
      minWidth: 100,
      maxWidth: 250
    }

    wrapper = shallow(
      <TableCell {...props}>
        lorem
      </TableCell>
    )
  })

  it('should set the correct width', () => {
    expect(wrapper.instance().styles).toEqual({ minWidth: props.minWidth, maxWidth: props.maxWidth })
    expect(wrapper.props().style).toEqual({ minWidth: props.minWidth, maxWidth: props.maxWidth })
  })
})

describe('<TableCell insideHeader />', () => {
  beforeEach(() => {
    props = {
      insideHeader: true
    }

    wrapper = shallow(
      <TableCell {...props}>
        lorem
      </TableCell>
    )
  })

  it('should be a <th />', () => {
    expect(wrapper.instance().tagName).toEqual('th')
    expect(wrapper.name()).toBe('th')
  })

  it('should render the correct class', () => {
    expect(wrapper.props().className).toEqual([
      styles.thContainer
    ].join(' '))
  })
})

describe('<TableCell with a custom className />', () => {
  beforeEach(() => {
    props = {
      className: 'lorem'
    }

    wrapper = shallow(
      <TableCell {...props}>
        lorem
      </TableCell>
    )
  })

  it('should render the correct class', () => {
    expect(wrapper.props().className).toEqual([
      styles.tdContainer,
      props.className
    ].join(' '))
  })
})

describe('<TableCell empty />', () => {
  beforeEach(() => {
    props = {
      empty: true
    }

    wrapper = mount(
      <TableCell {...props}>
        lorem
      </TableCell>
    )
  })

  it('should render the correct content', () => {
    expect(wrapper.find('i').length).toEqual(2)
  })
})

describe('<TableCell handlers />', () => {
  beforeEach(() => {
    props = {
      onClick: jest.fn().mockReturnValue({})
    }

    wrapper = mount(
      <TableCell {...props}>
        lorem
      </TableCell>
    )
  })

  it('should trigger the handlers', () => {
    wrapper.simulate('click')
    expect(props.onClick).toHaveBeenCalledTimes(1)
  })
})
