// @flow
import * as React from 'react'
import classnames from 'classnames'
import styles from './styles.scss'

type TProps = {
  theme?: 'primary' | 'default' | 'success' | 'danger' | 'link',
  disabled?: boolean,
  active?: boolean,
  hover?: boolean,
  preventToCloseOnClick?: boolean,

  className?: any,
  children: string | React.Node,

  onClick?: Function
}

class DropdownItem extends React.PureComponent<TProps> {
  static defaultProps = {
    theme: 'default',
    disabled: false,
    active: false,
    hover: false,
    preventToCloseOnClick: false,
    className: null,
    onClick: null
  }

  handleClick = (e: SyntheticEvent<HTMLDivElement>): void => {
    const { preventToCloseOnClick, disabled, onClick } = this.props
    !disabled && !preventToCloseOnClick && onClick && onClick()
  }

  render (): React.Node {
    const {
      theme,
      disabled,
      active,
      hover,
      className,
      children,
      preventToCloseOnClick,
      ...rest
    } = this.props

    const containerClassName = classnames(
      styles.dropdownItemContainer,
      [styles[theme]],
      { [styles.disabled]: disabled },
      { [styles.hover]: !disabled && hover },
      { [styles.active]: !disabled && active },
      className
    )

    return (
      <a
        tabIndex={-1}
        href="javascript:void(0);"
        className={containerClassName}
        {...rest}
        onClick={this.handleClick}
      >
        {children}
      </a>
    )
  }
}

export { styles }
export default DropdownItem
