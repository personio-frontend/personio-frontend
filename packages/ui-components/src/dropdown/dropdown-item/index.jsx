// @flow
import * as React from 'react'
import classnames from 'classnames'
import styles from './styles.scss'

type TProps = {
  theme?: 'primary' | 'default' | 'success' | 'danger' | 'link',
  disabled?: boolean,
  active?: boolean,
  hover?: boolean,
  preventToCloseOnClick?: boolean,
  className?: any,
  children: string | React.Node,
  onClick?: Function
}

class DropdownItem extends React.PureComponent<TProps> {
  static defaultProps = {
    theme: 'default',
    disabled: false,
    active: false,
    hover: false,
    preventToCloseOnClick: false,
    className: null,
    onClick: null
  }

  handleClick = (e: SyntheticEvent<HTMLDivElement>): void => {
    const { preventToCloseOnClick, disabled, onClick } = this.props
    !disabled && !preventToCloseOnClick && onClick && onClick()
  }

  render (): React.Node {
    const { theme, disabled, hover, active, className, children } = this.props

    const containerClassName = classnames(
      styles.dropdownItemContainer,
      [styles[theme]],
      { [styles.disabled]: disabled },
      { [styles.hover]: !disabled && hover },
      { [styles.active]: !disabled && active },
      className
    )

    return (
      <div
        className={containerClassName}
        onClick={this.handleClick}
        tabIndex={-1}
      >
        {children}
      </div>
    )
  }
}

export { styles }
export default DropdownItem
