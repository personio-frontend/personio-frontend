import * as React from 'react'
import DropdownItem, { styles } from './'

let props, wrapper

describe('<DropdownItem theme="success" />', () => {
  beforeEach(() => {
    props = {
      children: 'Foo',
      className: 'lorem',
      disabled: false,
      active: false,
      preventToCloseOnClick: false,
      onClick: jest.fn()
    }

    wrapper = shallow(<DropdownItem {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper).toHaveLength(1)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render the correct content', () => {
    expect(wrapper.text()).toEqual(props.children)
  })

  it('should call the handlers', () => {
    wrapper.simulate('click')
    expect(props.onClick).toHaveBeenCalledTimes(1)
  })

  it('should add a custom class', () => {
    expect(wrapper.props().className).toEqual([
      styles.dropdownItemContainer,
      styles.default,
      'lorem'
    ].join(' '))
  })
})

describe('<DropdownItem disabled active />', () => {
  beforeEach(() => {
    props = {
      children: 'Foo',
      theme: 'danger',
      disabled: true,
      active: true,
      onClick: jest.fn()
    }

    wrapper = shallow(<DropdownItem {...props} />)
  })

  it('should have the correct className', () => {
    expect(wrapper.props().className).toEqual([
      styles.dropdownItemContainer,
      styles.danger,
      styles.disabled
    ].join(' '))
  })
})

describe('<DropdownItem preventToCloseOnClick />', () => {
  beforeEach(() => {
    props = {
      children: 'Foo',
      preventToCloseOnClick: true,
      onClick: jest.fn()
    }

    wrapper = shallow(<DropdownItem {...props} />)
  })

  it('should not call the handlers when its disabled or has preventToCloseOnClick', () => {
    wrapper.simulate('click')
    expect(props.onClick).toHaveBeenCalledTimes(0)
  })
})

describe('<DropdownItem preventToCloseOnClick />', () => {
  beforeEach(() => {
    props = {
      children: 'Foo',
      disabled: true,
      preventToCloseOnClick: true,
      onClick: jest.fn()
    }

    wrapper = shallow(<DropdownItem {...props} />)
  })

  it('should not call the handlers when its disabled or has preventToCloseOnClick', () => {
    wrapper.simulate('click')
    expect(props.onClick).toHaveBeenCalledTimes(0)
  })
})

describe('<DropdownItem active />', () => {
  beforeEach(() => {
    props = {
      children: 'Foo',
      theme: 'primary',
      active: true,
      onClick: jest.fn()
    }

    wrapper = shallow(<DropdownItem {...props} />)
  })

  it('should have the correct className', () => {
    expect(wrapper.props().className).toEqual([
      styles.dropdownItemContainer,
      styles.primary,
      styles.active
    ].join(' '))
  })
})

describe('<DropdownItem hover />', () => {
  beforeEach(() => {
    props = {
      children: 'Foo',
      theme: 'primary',
      hover: true,
      onClick: jest.fn()
    }

    wrapper = shallow(<DropdownItem {...props} />)
  })

  it('should have the correct className', () => {
    expect(wrapper.props().className).toEqual([
      styles.dropdownItemContainer,
      styles.primary,
      styles.hover
    ].join(' '))
  })
})

describe('<DropdownItem disabled />', () => {
    beforeEach(() => {
      props = {
        children: 'Foo',
        disabled: true,
        onClick: jest.fn()
      }

      wrapper = shallow(<DropdownItem {...props} />)
    })

    it('should not call the handlers when its disabled or has preventToCloseOnClick', () => {
      wrapper.simulate('click')
      expect(props.onClick).toHaveBeenCalledTimes(0)
    })
})
