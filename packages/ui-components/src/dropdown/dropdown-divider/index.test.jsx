import * as React from 'react'
import DropdownDivider from './'

describe('<DropdownDivider theme="success" />', () => {
  let props, wrapper

  beforeEach(() => {
    props = {}
    wrapper = shallow(<DropdownDivider {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper).toHaveLength(1)
    expect(wrapper).toMatchSnapshot()
  })
})
