// @flow
import * as React from 'react'
import styles from './styles.scss'

type TProps = {}

function DropdownDivider (props: TProps) {
  return (
    <div className={styles.dropdownDividerContainer} />
  )
}

export default DropdownDivider
