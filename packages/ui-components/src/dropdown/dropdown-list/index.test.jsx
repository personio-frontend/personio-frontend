import * as React from 'react'
import DropdownList, { styles } from './'
import DropdownItem from '../dropdown-item'

let props, wrapper
describe('<DropdownList opened />', () => {
  beforeEach(() => {
    props = {
      open: true,
      className: 'lorem',
      direction: 'bottom-right'
    }

    wrapper = shallow(
      <DropdownList {...props}>
        <DropdownItem>Lorem</DropdownItem>
      </DropdownList>
    )
  })

  it('should render correctly', () => {
    expect(wrapper).toHaveLength(1)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render when its opened', () => {
    expect(wrapper.html()).not.toBe(null)
  })

  it('should return the correct style props', () => {
    expect(wrapper.instance().styleProps).toEqual({})
    wrapper.setProps({ offsetHeight: 32 })
    expect(wrapper.instance().styleProps).toEqual({
      transform: 'translate3d(0px, 32px, 0px)'
    })
  })

  it('should add a custom class', () => {
    expect(wrapper.props().className).toEqual([
      styles.dropdownListContainer,
      styles.bottomRight,
      'lorem'
    ].join(' '))
  })

  it('should have append the click prop to the children', () => {
    expect(wrapper.children().first().props().onClick).toBeDefined()
  })

  it('should have append the click prop to the children', () => {
    expect(wrapper.children().first().props().onClick).toBeDefined()
  })
})

describe('<DropdownList direction="bottom-left" />', () => {
  beforeEach(() => {
    props = {
      open: true,
      direction: 'bottom-left'
    }

    wrapper = shallow(<DropdownList {...props} />)
  })

  it('should add a custom class', () => {
    expect(wrapper.props().className).toEqual([
      styles.dropdownListContainer,
      styles.bottomLeft
    ].join(' '))
  })
})

describe('<DropdownList not opened />', () => {
  beforeEach(() => {
    props = {
      open: false,
      className: 'lorem'
    }

    wrapper = shallow(<DropdownList {...props} />)
  })

  it('should not render when its not opened', () => {
    expect(wrapper.html()).toBe(null)
  })
})
