// @flow
import * as React from 'react'
import classnames from 'classnames'
import camelCase from 'lodash/camelCase'
import styles from './styles.scss'

type TProps = {
  open?: boolean,
  className?: any,
  offsetHeight?: number,
  preventToCloseOnClick: boolean,
  direction: 'top-left' | 'top-right' | 'bottom-left' | 'bottom-right',

  children: React.Node,

  // Events
  onToggle?: Function
}

// TODO:
type TState = {
  currentItemIndexSelected: ?number
}

class DropdownList extends React.PureComponent<TProps, TState> {
  static defaultProps = {
    open: false,
    className: null,
    offsetHeight: null,
    preventToCloseOnClick: false,
    direction: 'bottom-right',
    onToggle: null
  }

  state = {
    currentItemIndexSelected: null
  }

  get styleProps (): Object {
    const { offsetHeight } = this.props
    let props = {}

    if (offsetHeight) {
      props.transform = `translate3d(0px, ${offsetHeight}px, 0px)`
    }

    return props
  }

  render (): React.Node {
    const {
      open,
      className,
      children,
      onToggle,
      preventToCloseOnClick,
      direction
    } = this.props

    if (!open) return null

    const containerClassName = classnames(
      styles.dropdownListContainer,
      [styles[camelCase(direction)]],
      className
    )

    return (
      <div
        className={containerClassName}
        style={this.styleProps}
      >
        {React.Children.map(children, (child) =>
          React.cloneElement(child, {
            onClick: child && child.props && child.props.onClick ? child.props.onClick : onToggle,
            preventToCloseOnClick
          })
        )}
      </div>
    )
  }
}

export { styles }
export default DropdownList
