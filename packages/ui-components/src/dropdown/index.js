/* istanbul ignore file */
import Dropdown from './dropdown'
import DropdownToggle from './dropdown-toggle'
import DropdownList from './dropdown-list'
import DropdownItem from './dropdown-item'
import DropdownDivider from './dropdown-divider'

Dropdown.Toggle = DropdownToggle
Dropdown.List = DropdownList
Dropdown.Item = DropdownItem
Dropdown.Divider = DropdownDivider

export {
  Dropdown,
  DropdownToggle,
  DropdownList,
  DropdownItem,
  DropdownDivider
}

export default Dropdown
