// @flow
import * as React from 'react'
import classnames from 'classnames'
import styles from './styles.scss'

type TProps = {
  children: string | React.Node,
  onToggle?: Function,
  className?: any
}

const ESC_KEY_CODE = 27

class DropdownToggle extends React.PureComponent<TProps> {
  static defaultProps = {
    onToggle: null,
    className: null
  }

  handleKeyDown = (e: SyntheticEvent<HTMLDivElement>): void => {
    const { keyCode } = e
    const { onToggle } = this.props

    if ((keyCode === ESC_KEY_CODE) && (onToggle)) onToggle()
  }

  render (): React.Node {
    const { className, onToggle, children } = this.props

    const containerClassName = classnames(
      styles.dropdownToggleContainer,
      className
    )

    return (
      <div
        className={containerClassName}
        onClick={onToggle}
        onKeyDown={this.handleKeyDown}
      >
        {children}
      </div>
    )
  }
}

export { styles }
export default DropdownToggle
