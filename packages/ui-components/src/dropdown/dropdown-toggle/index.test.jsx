import * as React from 'react'
import DropdownToggle, { styles } from './'

describe('<DropdownToggle theme="success" />', () => {
  let props, wrapper

  beforeEach(() => {
    props = {
      children: 'lorem',
      onToggle: jest.fn(),
      className: 'lorem'
    }

    wrapper = shallow(<DropdownToggle {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper).toHaveLength(1)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render the content', () => {
    expect(wrapper.text()).toEqual(props.children)
  })

  it('should call the click handler correctly', () => {
    expect(props.onToggle).toHaveBeenCalledTimes(0)
    wrapper.simulate('click')
    expect(props.onToggle).toHaveBeenCalledTimes(1)
    wrapper.simulate('click')
    expect(props.onToggle).toHaveBeenCalledTimes(2)
  })

  it('should call the click handler correctly', () => {
    expect(props.onToggle).toHaveBeenCalledTimes(0)
    wrapper.simulate('keyDown', { keyCode: 27 })
    expect(props.onToggle).toHaveBeenCalledTimes(1)
    wrapper.simulate('keyDown', { keyCode: 22 })
    expect(props.onToggle).toHaveBeenCalledTimes(1)
  })

  it('should add a custom class', () => {
    expect(wrapper.props().className).toEqual([
      styles.dropdownToggleContainer,
      'lorem'
    ].join(' '))
  })
})
