// @flow
import * as React from 'react'
import classnames from 'classnames'
import styles from './styles.scss'

type TProps = {
  children: React.Node,
  disabled?: boolean,
  open?: boolean,
  className?: any,

  // Events
  // Just will fire if the component is controlled
  onRequestToChange?: Function,

  // Just will fire if the component is uncontrolled
  onChange?: Function,
  beforeOpen?: Function,
  onOpen?: Function,
  beforeClose?: Function,
  onClose?: Function,
}

type TState = {
  open: boolean,
  offsetHeight: ?number
}

class Dropdown extends React.PureComponent<TProps, TState> {
  ref: ?HTMLDivElement;
  controlledComponent: boolean = false

  static defaultProps = {
    disabled: false,
    open: null
  }

  state = {
    open: false,
    offsetHeight: null
  }

  constructor (props) {
    super(props)

    const { open } = props

    // We use a internal state to control whether the dropdown is open or not,
    // but if the user set a initial prop, then, this prop will control the state
    if (open !== null) {
      this.controlledComponent = true
      this.state.open = props.open
    }
  }

  componentDidMount (): void {
    window.document.addEventListener('click', this._handleClickOutside)
  }

  componentWillUnmount (): void {
    window.document.removeEventListener('click', this._handleClickOutside)
  }

  /**
   * @description Return if the Dropdown is open or not based on state / prop
   * @returns { Boolean }
   */
  get open (): boolean {
    const { props, state, controlledComponent } = this
    if (controlledComponent) return props.open
    return state.open
  }

  /**
   * @description Indicate the correct method to been called after toggle the dropdown
   * @returns { Function }
   */
  get onToggleCallback (): Function {
    return this.controlledComponent ? this.props.onRequestToChange : this.handleToggle
  }

  _handleRef = (ref: ?HTMLDivElement): void => {
    /* istanbul ignore if  */
    if (ref) {
      this.ref = ref
      const offsetHeight = ref.offsetHeight
      this.setState({ offsetHeight })
    }
  }

  _handleClickOutside = (event: SyntheticEvent<HTMLElement>): void => {
    const { ref, open, onToggleCallback } = this

    /* $FlowFixMe */
    if (open && ref && !ref.contains(event.target)) {
      onToggleCallback && onToggleCallback()
    }
  }

  /**
   * @description If the component is controlled, we should fire an event to the user
   * indicating we want to toggle the state
   * @returns { Void }
   */
  handleRequestToChange = (): void => {
    const { controlledComponent } = this
    const { onRequestToChange, disabled } = this.props

    // Should be not disabled, controlled and have a event handler
    !disabled &&
      controlledComponent &&
        onRequestToChange &&
          onRequestToChange()
  }

  /**
   * @description Open the dropdown
   * @returns { Void }
   */
  handleOpen = (): void => {
    const { onChange, beforeOpen, onOpen, disabled } = this.props

    !disabled && beforeOpen && beforeOpen()
    this.setState({ open: true }, () => {
      if (!disabled) {
        onOpen && onOpen()
        onChange && onChange()
      }
    })
  }

  /**
   * @description Close the dropdown
   * @returns { Void }
   */
  handleClose = (): void => {
    const { onChange, beforeClose, onClose, disabled } = this.props

    !disabled && beforeClose && beforeClose()
    this.setState({ open: false }, () => {
      if (!disabled) {
        onClose && onClose()
        onChange && onChange()
      }
    })
  }

  /**
   * @description toggle the dropdown
   * @returns { Void }
   */
  handleToggle = (): void => {
    // eslint-disable-next-line
    const { handleOpen, handleClose } = this
    const method = this.state.open ? 'handleClose' : 'handleOpen'
    this[method]()
  }

  render (): React.Node {
    const { children, className } = this.props

    const containerClassName = classnames(
      styles.dropdownContainer,
      className
    )

    return (
      <div className={containerClassName} ref={this._handleRef}>
        {React.Children.map(children, (child) =>
          React.cloneElement(child, {
            open: this.open,
            onToggle: this.onToggleCallback,
            offsetHeight: this.state.offsetHeight
          })
        )}
      </div>
    )
  }
}

export { styles }
export default Dropdown
