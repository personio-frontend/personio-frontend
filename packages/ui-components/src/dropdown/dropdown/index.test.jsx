import React from 'react'
import { shallow } from 'enzyme'
import Dropdown, { styles } from './'
import DropdownToggle from '../dropdown-toggle'
import DropdownList from '../dropdown-list'
import DropdownItem from '../dropdown-item'

let wrapper
let props

describe('<Dropdown controlled />', () => {
  beforeEach(() => {
    window.document.addEventListener = jest.fn()
    window.document.removeEventListener = jest.fn()

    props = {
      open: false,
      onRequestToChange: jest.fn(),
      className: 'lorem'
    }

    wrapper = shallow(
      <Dropdown {...props}>
        <DropdownToggle>foo</DropdownToggle>
        <DropdownList>
          <DropdownItem>lorem</DropdownItem>
        </DropdownList>
      </Dropdown>
    )
  })

  it('should render correctly', () => {
    expect(wrapper).toMatchSnapshot()
    expect(wrapper).toHaveLength(1)
  })

  it('should add listeners on componentDidMount', () => {
    expect(window.document.addEventListener).toHaveBeenCalled()
  })

  it('should remove listeners on componentWillUnmount', () => {
    expect(window.document.removeEventListener).not.toHaveBeenCalled()
    wrapper.instance().componentWillUnmount()
    expect(window.document.removeEventListener).toHaveBeenCalled()
  })

  it('should indicate the component is controlled', () => {
    expect(wrapper.instance().controlledComponent).toBe(true)
  })

  it('should indicate the correct open state', () => {
    expect(wrapper.instance().open).toBe(false)
    wrapper.setProps({ open: true })
    expect(wrapper.instance().open).toBe(true)
  })

  it('should call handleRequestToChange handler', () => {
    expect(props.onRequestToChange).toHaveBeenCalledTimes(0)
    wrapper.instance().handleRequestToChange()
    expect(props.onRequestToChange).toHaveBeenCalledTimes(1)
  })

  it('should not call handleRequestToChange handler when its disabled', () => {
    wrapper.setProps({ disabled: true })
    expect(props.onRequestToChange).toHaveBeenCalledTimes(0)
    wrapper.instance().handleRequestToChange()
    expect(props.onRequestToChange).toHaveBeenCalledTimes(0)
  })

  it('should indicate the correct handler after toggle', () => {
    wrapper.instance().handleToggle = jest.fn()

    expect(props.onRequestToChange).toHaveBeenCalledTimes(0)
    expect(wrapper.instance().handleToggle).toHaveBeenCalledTimes(0)

    wrapper.instance().onToggleCallback()

    expect(props.onRequestToChange).toHaveBeenCalledTimes(1)
    expect(wrapper.instance().handleToggle).toHaveBeenCalledTimes(0)
  })

  it('should call handleToggle after click outside the component when its opened', () => {
    wrapper.setProps({ open: true })
    wrapper.instance().ref = { contains: jest.fn() }

    expect(props.onRequestToChange).not.toHaveBeenCalled()
    wrapper.instance()._handleClickOutside({ target: jest.fn() })
    expect(props.onRequestToChange).toHaveBeenCalled()
  })

  it('should not call handleToggle after click outside the component when its closed', () => {
    wrapper.instance().ref = { contains: jest.fn() }

    expect(props.onRequestToChange).not.toHaveBeenCalled()
    wrapper.instance()._handleClickOutside({ target: jest.fn() })
    expect(props.onRequestToChange).not.toHaveBeenCalled()
  })

  it('should add a custom class', () => {
    expect(wrapper.props().className).toEqual([
      styles.dropdownContainer,
      'lorem'
    ].join(' '))
  })
})

describe('<Dropdown not controlled>', () => {
  beforeEach(() => {
    props = {
      onChange: jest.fn(),
      beforeOpen: jest.fn(),
      onOpen: jest.fn(),
      beforeClose: jest.fn(),
      onClose: jest.fn(),
      onRequestToChange: jest.fn()
    }

    wrapper = shallow(<Dropdown {...props} />)
  })

  it('should indicate the component is not controlled', () => {
    expect(wrapper.instance().controlledComponent).toBe(false)
  })

  it('should indicate the correct open state', () => {
    expect(wrapper.instance().open).toBe(false)
    wrapper.setState({ open: true })
    expect(wrapper.instance().open).toBe(true)
  })

  it('should call the the open handlers', () => {
    expect(props.beforeOpen).toHaveBeenCalledTimes(0)
    expect(props.onOpen).toHaveBeenCalledTimes(0)
    expect(props.onChange).toHaveBeenCalledTimes(0)

    wrapper.instance().handleOpen()

    expect(props.beforeOpen).toHaveBeenCalledTimes(1)
    expect(props.onOpen).toHaveBeenCalledTimes(1)
    expect(props.onChange).toHaveBeenCalledTimes(1)

    wrapper.setProps({ disabled: true })
    wrapper.instance().handleOpen()

    expect(props.beforeOpen).toHaveBeenCalledTimes(1)
    expect(props.onOpen).toHaveBeenCalledTimes(1)
    expect(props.onChange).toHaveBeenCalledTimes(1)
  })

  it('should call the the close handlers', () => {
    expect(props.beforeClose).toHaveBeenCalledTimes(0)
    expect(props.onClose).toHaveBeenCalledTimes(0)
    expect(props.onChange).toHaveBeenCalledTimes(0)

    wrapper.instance().handleClose()

    expect(props.beforeClose).toHaveBeenCalledTimes(1)
    expect(props.onClose).toHaveBeenCalledTimes(1)
    expect(props.onChange).toHaveBeenCalledTimes(1)

    wrapper.setProps({ disabled: true })
    wrapper.instance().handleClose()

    expect(props.beforeClose).toHaveBeenCalledTimes(1)
    expect(props.onClose).toHaveBeenCalledTimes(1)
    expect(props.onChange).toHaveBeenCalledTimes(1)
  })

  it('should call the correct handler when it toggles', () => {
    wrapper.instance().handleClose = jest.fn()
    wrapper.instance().handleOpen = jest.fn()

    expect(wrapper.instance().handleOpen).toHaveBeenCalledTimes(0)
    expect(wrapper.instance().handleClose).toHaveBeenCalledTimes(0)

    wrapper.instance().handleToggle()
    wrapper.setState({ open: true })
    expect(wrapper.instance().handleOpen).toHaveBeenCalledTimes(1)
    expect(wrapper.instance().handleClose).toHaveBeenCalledTimes(0)

    wrapper.instance().handleToggle()
    wrapper.setState({ open: false })
    expect(wrapper.instance().handleOpen).toHaveBeenCalledTimes(1)
    expect(wrapper.instance().handleClose).toHaveBeenCalledTimes(1)
  })

  it('should indicate the correct handler after toggle', () => {
    wrapper.instance().handleToggle = jest.fn()

    expect(props.onRequestToChange).toHaveBeenCalledTimes(0)
    expect(wrapper.instance().handleToggle).toHaveBeenCalledTimes(0)

    wrapper.instance().onToggleCallback()

    expect(props.onRequestToChange).toHaveBeenCalledTimes(0)
    expect(wrapper.instance().handleToggle).toHaveBeenCalledTimes(1)
  })
})
