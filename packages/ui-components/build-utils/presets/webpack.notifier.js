const WebpackNotifierPlugin = require('webpack-notifier')

module.exports = () => ({
  plugins: [new WebpackNotifierPlugin()]
})
