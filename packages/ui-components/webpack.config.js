const webpack = require('webpack')
const path = require('path')
const webpackMerge = require('webpack-merge')
const CleanWebpackPlugin = require('clean-webpack-plugin')

const loadPresets = require('./build-utils/loadPresets')
const modeConfig = env => require(`./build-utils/webpack.${env.mode}.js`)(env)

module.exports = ({ mode, presets } = { mode: 'production', presets: [] }) => {
  const config = webpackMerge(
    {
      mode,
      entry: [path.join(__dirname, 'index.js')],
      resolve: {
        modules: [path.join(__dirname, 'node_modules'), path.join(__dirname)],
        extensions: ['.js', '.jsx']
      },
      module: {
        rules: [
          {
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: {
              options: {
                cwd: path.join(__dirname)
              },
              loader: 'babel-loader'
            }
          },
          {
            test: /\.jpeg$/,
            use: [
              {
                loader: 'url-loader',
                options: {
                  limit: 5000
                }
              }
            ]
          }
        ]
      },
      output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
        library: '@personio/ui-components',
        libraryTarget: 'umd',
        umdNamedDefine: true
      },
      plugins: [
        new webpack.ProgressPlugin(),
        new CleanWebpackPlugin(
          ['dist', 'build']
        )
      ]
    },
    modeConfig({ mode, presets }),
    loadPresets({ mode, presets })
  )

  console.log(config)

  return config
}
