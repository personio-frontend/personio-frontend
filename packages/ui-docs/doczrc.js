import { css } from 'docz-plugin-css'

export default {
  title: 'Personio UI',
  description: 'Personio components',
  htmlContext: {
    head: {
      links: [{
        rel: 'stylesheet',
        href: '//fonts.googleapis.com/css?family=Open+Sans'
      }, {
        ref: 'stylesheet',
        href: 'https://pro.fontawesome.com/releases/v5.0.10/css/all.css'
      }]
    }
  },
  menu: [
    {
      name: 'Overview',
      menu: ['Introduction', 'Principles']
    },
    {
      name: 'Components',
      menu: ['Icon', 'Button', 'Dropdown', 'Table']
    }
  ],
  plugins: [
    css({
      preprocessor: 'postcss',
      cssmodules: true,
      loaderOpts: {
        /* whatever your preprocessor loader accept */
      }
    })
  ]
}